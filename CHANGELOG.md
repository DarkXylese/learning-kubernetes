## 0.1.0 (2024-04-01)

### BREAKING CHANGE

- New Feature

### Feat

- **golant-tg-bot**: Added initial version of the golang telegram bot
