Build project:
    go build -o bin/app src/main.go

Build docker (most commands req sudo):

    docker build .

    docker run -p 8080:8080 <id>

    docker tag <id> darkxylese/k8project1:v1.0.0

    # Create small local k8 cluster
    k3d cluster create k8-project-1
    kubectl cluster-info

    kubectl create deploy k8-project-1 --image darkxylese/k8project1:v1.0.0

    # Local Portforwarding
    kubectl get po -w
    sudo kubectl port-forward deploy/k8-project-1 8080

    # Lets update our docker with html changes
    docker build . -t darkxylese/k8project1:v1.0.1

    # 0 Downtime deployment of changes
    kubectl get deploy k8-project-1 -o wide
    kubectl set image deploy k8-project-1 k8project1=darkxylese/k8project1:v1.0.1

    #Stop k3d cluster
    k3d cluster stop -a