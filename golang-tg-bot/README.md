Making the project:

    go mod init gobot

    go install github.com/spf13/cobra-cli@latest

    (Fix paths by adding 'export PATH=$PATH:$GOPATH/bin' to bashrc, as well as 'export $GOPATH' after checking go env, if that isn't set correctly on install)

    cobra-cli init

    cobra-cli add version

    go run main.go help

    cobra-cli add gobot

    gofmt -s -w ./

    go get

    go build -ldflags "-X=gobot/cmd.appVersion=v1.0.1"
