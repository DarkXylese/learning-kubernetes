/*
Copyright © 2024 DarkXylese
*/
package main

import "gobot/cmd"

func main() {
	cmd.Execute()
}
