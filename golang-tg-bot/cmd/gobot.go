/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"

	tb "gopkg.in/telebot.v3"
)

var (
	TeleToken = os.Getenv("TELE_TOKEN")
)

// gobotCmd represents the gobot command
var gobotCmd = &cobra.Command{
	Use:   "gobot",
	Aliases: []string{"start"},
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {

		fmt.Printf("Gobot %s started", appVersion)
		gobot, err := tb.NewBot(tb.Settings{
			URL:    "",
			Token:  TeleToken,
			Poller: &tb.LongPoller{Timeout: 10 * time.Second},
		})

		if err != nil {
			log.Fatalf("Most likely and error with TELE_TOKEN env var being missing. %s", err)
			return
		}

		gobot.Handle(tb.OnText, func(m tb.Context) error {
			log.Print(m.Message().Payload, m.Text())
			payload := m.Message().Payload
			
			switch payload {
				case "hello":
				err = m.Send(fmt.Sprintf("Hello I'm Gobot %s", appVersion))
			}
			return err
		})

		// gobot.Handle(tb.OnMessage, func(m *tb.Message) {
		// 	b.Send(m.Sender, "hello world")
		// }

		gobot.Start()

	},
}

func init() {
	rootCmd.AddCommand(gobotCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// gobotCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// gobotCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
